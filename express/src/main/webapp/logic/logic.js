var logic={
		userdata:{name:"Sanjeev"},
		ratingdata:{pplVoted:120,avgRanking:4.3,userVoted:[]},
		speechStat:[{data:50,label:"5 star"},{data:45,label:"4 star"},{data:10,label:"3 star"},{data:10,label:"2 star"},{data:5,label:"1 star"}],
		currentQuestion:{pplVoted:120,Y:34,N:86},
		audio:{isAudioComment:false,audio_url:[]},
		getSpeechData:function(){
			$.ajax({
				url: '../../../model/speechdata.json',
				type: 'get',
				dataType: 'json',
				error: function(data){
					alert("oops something went wrong.");
				},
				success: function(data){
				 var  html=textspeechtemplate(data,{"data":this.userdata});
				 var  commentsection = commenttemplate(logic.userdata);
				 var ratingsection = ratespeech();
				 $('#ratingsection').html(ratingsection);
				 $('#speechsection').html(html);
				 $('#commentsection').html(commentsection);
				 myuploadfunction();
				 logic.plotPollGrapgh();
				 logic.getCurrentPollData();
				    }
				});
		},
		submitComment : function(component){
			var value = $("#commenttextarea").val();
			$("#commenttextarea").val('');
			var comment={"commenttext":value,"username":this.userdata.name,"audio":this.audio};
			var html=viewercommenttemplate(comment);
			$("#viewerscomment").append(html);
		},
		setRating:function(component){
			var user = logic.userdata.name;
			if($.inArray(user, logic.ratingdata.userVoted) === -1){
				var nthstar=parseInt(component.getAttribute('rating'));
				logic.ratingdata.userVoted.push(user);
				logic.ratingdata.pplVoted+=1;
				logic.ratingdata.avgRanking=((logic.ratingdata.avgRanking * logic.ratingdata.pplVoted) + nthstar)/logic.ratingdata.pplVoted;
			 $('#avgrating').text(logic.ratingdata.avgRanking.toFixed(2));
			for(var i=1;i<=5;i++){
				$('[rating="'+i+'"]').removeClass('starclicked');
			}
			for(var i=1;i<=nthstar;i++){
				$('[rating="'+i+'"]').addClass('starclicked');
			}
			
			
				
			}else{
				alert('Already Voted');
				return true;
			}
		},
		plotPollGrapgh:function(){
			
			var data = [],
			data=this.speechStat;
		/*	series = Math.floor(Math.random() * 6) + 3;

		for (var i = 0; i < series; i++) {
			data[i] = {
				label: "Series" + (i + 1),
				data: Math.floor(Math.random() * 100) + 1
			};
		}*/

		var placeholder = $("#chartplaceholder");

		/*$("#example-1").click(function() {

			placeholder.unbind();

			$("#title").text("Default pie chart");
			$("#description").text("The default pie chart with no options set.");*/

			$.plot(placeholder, data, {
				series: {
					pie: { 
						 innerRadius: 0.5,
						show: true
					}
				},
			legend: {
				       show: false
				   }
			});
		},
		getCurrentPollData: function(){
			$.ajax({
				url: '../../../model/currentpolldata.json',
				type: 'get',
				dataType: 'json',
				error: function(data){
					alert("oops something went wrong in fetching poll data.");
				},
				success: function(data){
				 var  html=polltemplate(data,{"data":this.userdata});
				 $('#currentpoll').html(html);
				    }
				});
		},
		storeVote:function(){
		//	var votedradio=$("input[name=PDI_answer8311135]:checked").val();
			
		},
		showresult:function(component){
			$(component).hide();
			$("#firstoption").empty();
			$("#secondoption").empty();
			$( "#firstoption" ).slider({
			      value: 60,
			      orientation: "horizontal",
			      range: "min",
			      animate: true
			    });
			$( "#secondoption" ).slider({
			      value: 60,
			      orientation: "horizontal",
			      range: "min",
			      animate: true
			    });
		},
		setLanguage :function(lang){
			language= lang;
			$.ajax({

				url: '../../../content/label_'+lang+'.json',
				type: 'get',
				dataType: 'json',
				error: function(data){
					alert("oops something went wrong in fetching label.");
				},
				success: function(data){ 
					logic.label=data;
					window.location.href="#setpoll";
				}
				
			});
					},
		 myuploadfunction :function(){
		    $('#fileupload').fileupload({
		        dataType: 'json',
		 
		        done: function (e, data) {
		            //$("tr:has(td)").remove();
		            $.each(data.result, function (index, file) {
		            	 logic.audio.isAudioComment=true;
		                 logic.audio.audio_url.push("../get/"+index);
		            });
		                /*$("#uploaded-files").append(
		                        $('<tr/>')
		                        .append($('<td/>').text(file.fileName))
		                        .append($('<td/>').text(file.fileSize))
		                        .append($('<td/>').text(file.fileType))
		                        .append($('<td/>').html("<a href='rest/controller/get/"+index+"'>Click</a>"))
		                        .append($('<td/>').html("<audio controls><source src='rest/controller/get/"+index+"' type='audio/mpeg'></audio>")));//end $("#uploaded-files").append()
		            }); 
		        	$('#uploded-files').text(file.fileName);*/
		        },
		 
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
		            $('#progress .bar').css(
		                'width',
		                progress + '%'
		            );
		        },
		 
		        dropZone: $('#dropzone')
		    });
		}
		
};
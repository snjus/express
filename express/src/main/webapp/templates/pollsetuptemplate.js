var pollsetuptemplate = Handlebars.compile(

"<div id='pollsetupcontainer' class='container'>"
		
		+"<div class='row' >"
		
		+"<div class='col-md-12'>"
		+"<form role='form' id='pollsetupform' method='POST'>"
		
		+"<div class='form-group'>"
		+"<label for='speaker'>{{speaker}}: </label>"
        +"<input id='speaker' name='speaker' type='text' value='' class='form-control'><br/>"
        +"</div>"
        
        +"<div class='form-group'>"
        +"<label for='date' >{{date}}: </label>"
        +"<input id='date' name='date' type='text' value='' class='form-control'> <br/>"
        +"</div>"
        
        +"<div class='form-group'>"
        +"<label for='place'>{{place}}: </label>"
        +"<input id='place' name='place' type='text' value='' class='form-control'> <br/>"
        +"</div>"
        
        +"<div class='form-group'>"
        +"<label for='topic'>{{speech_topic}}: </label>"
        +"<input id='topic' name='Topic' type='text' value='' class='form-control'> <br/>"
        +"</div>"
        
        +"<div class='form-group'>"
        +"<label for='text'>{{speech_text}}: </label>"
        +"<input id='text' name='text' type='textarea' value='' class='form-control'> <br/>"
        +"</div>"
        
        +"<div class='form-group'>"
        +"<label for'fileupload'>{{upload_audio}}: </label>"
        +"<input id='fileupload' type='file' name='files[]' data-url='../upload' multiple /> <br/>"
        +"</div>"
        
        +"<div class='form-group'>"
        +"<label for='videolink'>{{you_tube_link}}: </label>"
        +"<input id='videolink' name='video' type='text' value='' class='form-control'> <br/>"
        +"</div>"
        
        +"<button  type='submit' class='btn btn-primary'  >{{submit}}</button>"
		+"</form>"
		
		
		+"</div>"
		
		
		+"</div>"
		 
		 
		 
		
		
		
		 
		
		+"</div>"









);
var languageselecttemplate= Handlebars.compile(
		
		"<div class='form-group' id='languagebox'>" +
		"<label for='language'>Select language</label>" +
		"<select  class='form-control'  id ='language' onchange='logic.setLanguage(this.value);'>" +
		"<option value='--EMPTY--'>Select</option>" +
		"<option value='english'>English</option>" +
		"<option value='hindi'>Hindi</option>" +
		"<option value='bhojpuri'>Bhojpuri</option>" +
		"</select>" +
		"</div>"

);